from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS
from pprint import pprint
app = Flask(__name__)
cors = CORS(app)
app.config["MONGO_URI"] = "mongodb://localhost:27017/huastecappdb"
mongo = PyMongo(app)

@app.route("/")
def hello():
    post = {
        "nombre":"mothy",
    }
    user = mongo.db.prueba.insert_one(post)
    return user
@app.route("/registrar/<usuario>")
def registrar(usuario):
    import json
    data = json.loads(usuario)
    existencia = mongo.db.usuarios.find_one({"usuario":data["usuario"]})
    if data['clave'] != data['clavecomp']:
        return "Las contraseñas no coinciden"
    elif data['nombrecomp'] == '' or data['clave'] == '' or data['usuario'] == '':
        return "Porfavor rellene todos los campos"
    elif existencia != None:
        return "El usuario ya existe"
    else:
        try:
            user = mongo.db.usuarios.insert_one(data)
            pass
            return "Registro Exitoso"
        except Exception as e:
            return "Hubo un error en su registro"
            raise

@app.route("/login/<usuario>")
def login(usuario):
    import json
    data = json.loads(usuario)
    user = mongo.db.usuarios.find_one({"usuario":data["usuario"]})
    if user == None:
        return "Usuario y/o contraseña incorrectos"
    elif user["clave"] == data["clave"]:
        return "Bienvenido"
    else:
        return "Usuario y/o contraseña incorrectos"
