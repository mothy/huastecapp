import $axios from 'axios'
const LOCAL_BASE_URL = 'http://localhost:5000';

export const registro=(state,user)=>{
    return $axios(`${LOCAL_BASE_URL}/registrar/${user}`).then(res=>res.data)
}
export const login=(state,user)=>{
    return $axios(`${LOCAL_BASE_URL}/login/${user}`).then(res=>res.data)
}
